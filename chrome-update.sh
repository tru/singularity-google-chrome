#!/bin/sh
d=`mktemp -d /dev/shm/chromeXXXX`
S=~/singularity-img/chrome.img
echo "running non persistant google-chrome in $d"
if [ -d $d ]; then
singularity exec -B /run -H $d $S bash -c 'apt-get update && apt-get -y upgrade'
echo "removing $d"
/bin/rm -rf $d
fi

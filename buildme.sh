#!/bin/sh
# I assume that singularity is installed as /usr/bin/singularity
# and the singularity containers images are in ~/singularity-img
PATH=/usr/bin:/usr/sbin:/bin
if [ -f /usr/bin/singularity ] && [ ! -f ~/singularity-img/chrome.img ] && [ -f singularity-debian-amd64-jessie-fr-google-chrome.def ]; then
sudo singularity create --size 2000  ~/singularity-img/chrome.img && \
sudo singularity bootstrap ~/singularity-img/chrome.img singularity-debian-amd64-jessie-fr-google-chrome.def && \
echo  ~/singularity-img/chrome.img built
fi

#!/bin/sh
d=$HOME/.singularity/chrome
S=~/singularity-img/chrome.img
echo "running persistant google-chrome in $d"
if [ -d $d ]; then
singularity exec -B /run $S /opt/google/chrome/google-chrome --no-sandbox --user-data-dir=$d
fi

I am running a CentOS-7 laptop and I want to isolate google chrome from my host $HOME.
Furthermore I want to always start from a fresh configuration, not attached to any 
google accounts.

1. install singularity
2. build the container with
  * sudo singularity create --size 2000 chrome.img && sudo singularity bootstrap chrome.img singularity-debian-amd64-jessie-fr-google-chrome.def
  * or run ./buildme.sh
3. use/adapt either chrome.sh or chrome-private.sh  and use them

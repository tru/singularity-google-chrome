#!/bin/sh
d=`mktemp -d /dev/shm/chromeXXXX`
S=~/singularity-img/chrome.img
echo "running non persistant google-chrome in $d"
if [ -d $d ]; then
singularity exec -B /run -H $d $S /opt/google/chrome/google-chrome \
--bwsi \
--disable-metrics \
--no-first-run \
--no-sandbox \
--disable-translate \
"$@"
echo "removing $d"
/bin/rm -rf $d
fi
